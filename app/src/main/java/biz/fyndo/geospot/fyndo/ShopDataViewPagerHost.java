package biz.fyndo.geospot.fyndo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vibinvarghese on 29/09/16.
 */

public class ShopDataViewPagerHost extends AppCompatActivity {


    private TabLayout tabLayout;
    private ViewPager viewPager;

    PostFragment postFragment;
    ProfileInfoFragment profileInfoFragment;
    FeedsFragment feedsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shop_data_pager);

        postFragment = new PostFragment();
        profileInfoFragment = new ProfileInfoFragment(true);
        feedsFragment = new FeedsFragment();

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        LinearLayout tabOne = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab_item, null);
        ((TextView) (tabOne.findViewById(R.id.title))).setText("PROFILE");
        ((ImageView) (tabOne.findViewById(R.id.icon))).setImageResource(R.drawable.profile_tab);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        LinearLayout tabTwo = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab_item, null);
        ((TextView) (tabTwo.findViewById(R.id.title))).setText("POST");
        ((ImageView) (tabTwo.findViewById(R.id.icon))).setImageResource(R.drawable.post_tab);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        LinearLayout tabThree = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab_item, null);
        ((TextView) (tabThree.findViewById(R.id.title))).setText("FEED");
        ((ImageView) (tabThree.findViewById(R.id.icon))).setImageResource(R.drawable.feed_tab);
        tabLayout.getTabAt(2).setCustomView(tabThree);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                ((ImageView) (tabLayout.getTabAt(tab.getPosition()).getCustomView().findViewById(R.id.icon))).setColorFilter(getResources().getColor(R.color.colorPrimary));
                ((TextView) (tabLayout.getTabAt(tab.getPosition()).getCustomView().findViewById(R.id.title))).setTextColor(getResources().getColor(R.color.colorPrimary));

                if (tab.getPosition() == 1) {
                    /*if (postFragment.image2 != null)
                        postFragment.image2.setVisibility(View.GONE);
                    if (postFragment.image3 != null)
                        postFragment.image3.setVisibility(View.GONE);*/

                    //postFragment.updateShopData();
                }
                if (tab.getPosition() == 2) {
                    feedsFragment.updateShopData();
                }
                if (tab.getPosition() == 0) {
                    profileInfoFragment.prefillData();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                ((ImageView) (tabLayout.getTabAt(tab.getPosition()).getCustomView().findViewById(R.id.icon))).setColorFilter(getResources().getColor(R.color.divider));
                ((TextView) (tabLayout.getTabAt(tab.getPosition()).getCustomView().findViewById(R.id.title))).setTextColor(getResources().getColor(R.color.divider));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        viewPager.setCurrentItem(1, true);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 456) {
            postFragment.processImageCapture(requestCode, resultCode, data);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(profileInfoFragment, "Profile");
        adapter.addFragment(postFragment, "Post");
        adapter.addFragment(feedsFragment, "Feed");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    protected void postUserBizInfoUpdated(String response, boolean isSuccess) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                profileInfoFragment.progressBar.setVisibility(View.GONE);
                profileInfoFragment.actionButton.setVisibility(View.VISIBLE);
            }
        });

        if (!isSuccess) {
            Util.showSnackBar("Something went wrong. Please try again.", profileInfoFragment.shopPhone);
            return;
        }

        Util.showSnackBar("Profile Updated Successfully", profileInfoFragment.shopPhone);
    }

    protected void getFeedsFromUserId(String response, boolean isSuccess) {


        if (!isSuccess) {
            Util.showSnackBar("Something went wrong. Please try again.", feedsFragment.shopArea);
            return;
        }

        feedsFragment.populateFetchedFeeds(response, isSuccess);
    }
    protected void postDeleteFeed(String response, boolean isSuccess) {


        if (!isSuccess) {
            Util.showSnackBar("Something went wrong. Please try again.", feedsFragment.shopArea);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    feedsFragment.updateShopData();
                }
            });
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                feedsFragment.updateShopData();
            }
        });
    }

    protected void postFeedCompleted(String response, boolean isSuccess) {

        if (!isSuccess) {
            Util.showSnackBar("Something went wrong. Please try again.", postFragment.postButton);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    postFragment.progressBar.setVisibility(View.GONE);
                    postFragment.postButton.setVisibility(View.VISIBLE);
                }
            });
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                postFragment.content.setText("");
                postFragment.title.setText("");
                /*postFragment.postTitle.setText("");
                postFragment.duration.setText("1 days(s)");*/
                postFragment.image1.setImageDrawable(getResources().getDrawable(R.drawable.image_place_holder));
                postFragment.image2.setImageDrawable(getResources().getDrawable(R.drawable.image_place_holder));
                postFragment.image3.setImageDrawable(getResources().getDrawable(R.drawable.image_place_holder));
                postFragment.progressBar.setVisibility(View.GONE);
                postFragment.postButton.setVisibility(View.VISIBLE);

                viewPager.setCurrentItem(2, true);
                feedsFragment.updateShopData();
            }
        });

        Util.showSnackBar("Post Uploaded Successfully", postFragment.postButton);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
