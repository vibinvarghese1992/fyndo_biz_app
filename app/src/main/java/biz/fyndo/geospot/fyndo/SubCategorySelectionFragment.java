package biz.fyndo.geospot.fyndo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vibinvarghese on 09/01/17.
 */

public class SubCategorySelectionFragment extends Fragment {


    CardView header;

    ArrayList<String> selectedMen = new ArrayList<>();
    ArrayList<String> selectedWomen = new ArrayList<>();

    ArrayList<String> men = new ArrayList<>();
    ArrayList<String> women = new ArrayList<>();

    ArrayList<String> alreadySelectedCats = new ArrayList<>();

    ProgressBar progressBar;
    ScrollView mainScroll;
    ImageView tick;
    SharedPreferences prefs;
    View rootView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.sub_category_picker_scroll_fragment, container, false);

        men.clear();
        men.add("Casual Shirts");
        men.add("Casual Shoes");
        men.add("Casual Trousers");
        men.add("Formal Shirts");
        men.add("Formal Shoes");
        men.add("Formal Trousers");
        men.add("Indian & Festive Wear");
        men.add("Sandals & Floaters");
        men.add("Shorts");
        men.add("Sports Shoes");
        men.add("T-Shirts");
        men.add("Watches & Wearables");

        women.clear();
        women.add("Boutique & Fashion Jewellery");
        women.add("Flats & Casual Shoes");
        women.add("Handbags, Bags & Wallets");
        women.add("Indian & Fusion Wear");
        women.add("Watches & Wearables");
        women.add("Western Wear");

        header = (CardView) rootView.findViewById(R.id.header);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress);
        mainScroll = (ScrollView) rootView.findViewById(R.id.main_scroll);
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        tick = (ImageView) rootView.findViewById(R.id.tick);
        tick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processServerResponse("", true);
            }
        });

        CheckBox selectAllMen = (CheckBox) rootView.findViewById(R.id.select_all_men);
        CheckBox selectAllWomen = (CheckBox) rootView.findViewById(R.id.select_all_women);

        selectAllMen.setOnCheckedChangeListener(checkedChangeListener);
        selectAllWomen.setOnCheckedChangeListener(checkedChangeListener);

        /*if (prefs.getInt(Constants.BIZ_AREA_LIST, 0) > 0) {
            progressBar.setVisibility(View.VISIBLE);
            mainScroll.setVisibility(View.GONE);

            PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "AlreadySelectedCats",
                    Constants.SELECT_BIZ_CATEGORIES + prefs.getInt(Constants.BIZ_ID, 0), null, "GET");

            performAsyncTask.execute();

        } else {*/
            insertPreferences((FlexboxLayout) rootView.findViewById(R.id.men_main), R.id.men, men, Constants.USER_PREFERENCES_MEN);
            insertPreferences((FlexboxLayout) rootView.findViewById(R.id.women_main), R.id.women, women, Constants.USER_PREFERENCES_WOMEN);
        //}

        return rootView;
    }

    protected void processAlreadySelectedCats(String response, boolean isSuccess) {
        if (!isSuccess) {
            Util.showSnackBar("Something went wrong, please try again", progressBar);
            return;
        }

        JSONArray cats = null;
        try {
            cats = new JSONArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < cats.length(); i++) {
            JSONObject obj = null;
            try {
                obj = cats.getJSONObject(i);

                if (obj.getString("bizCategory").equals("null"))
                    alreadySelectedCats.add(obj.getString("userCatLevel2").replace("\\u0026", "&"));
                else
                    alreadySelectedCats.add(obj.getString("bizCategory").replace("\\u0026", "&"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        progressBar.setVisibility(View.GONE);
        tick.setVisibility(View.VISIBLE);
        mainScroll.setVisibility(View.VISIBLE);

        insertPreferences((FlexboxLayout) rootView.findViewById(R.id.men_main), R.id.men, men, Constants.USER_PREFERENCES_MEN);
        insertPreferences((FlexboxLayout) rootView.findViewById(R.id.women_main), R.id.women, women, Constants.USER_PREFERENCES_WOMEN);

    }

    protected void createOrUpdateBizObject() {
        tick.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        mainScroll.setVisibility(View.GONE);

       /* if (((MainActivity) getActivity()).shopImageBitampCaptured != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ((MainActivity) getActivity()).shopImageBitampCaptured.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArray = baos.toByteArray();

            PerformImageUpload performImageUpload = new PerformImageUpload(getActivity(), byteArray, null, null, "shop_image.jpg");
            performImageUpload.execute();
        } else {
            postImageUpload("", true);
        }*/

        Gson gson = new Gson();

        JSONObject postDataParams = new JSONObject();

        Location location = Util.getLocation(getActivity());

        try {

            JSONArray shopCategories = new JSONArray();
            JSONObject men = new JSONObject();
            men.put("Men", gson.toJson(selectedMen));
            JSONObject women = new JSONObject();
            women.put("Women", gson.toJson(selectedWomen));
            shopCategories.put(men);
            shopCategories.put(women);

            if (prefs.getInt(Constants.BIZ_ID, 0) > 0) {
                postDataParams.put("bizId", prefs.getInt(Constants.BIZ_ID, 0));
            }


            gson = new Gson();
            String json = prefs.getString(Constants.BIZ_AREA_LIST, null);
            Type type = new TypeToken<List<String>>() {
            }.getType();
            ArrayList<String> shopAreaList = gson.fromJson(json, type);
            int shopAreaListPosition = prefs.getInt(Constants.SHOP_AREA, 0);

            postDataParams.put("gpsLatitude", location.getLatitude());
            postDataParams.put("gpsLongitude", location.getLongitude());
            postDataParams.put("bizName", prefs.getString(Constants.SHOP_NAME, "Shop Name"));
            postDataParams.put("bizStreetAddress", shopAreaList.get(shopAreaListPosition));
            postDataParams.put("bizMobile", prefs.getString(Constants.SHOP_PHONE, ""));
            postDataParams.put("bizCategory", shopCategories.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        PerformAsyncTask performAsyncTask;

        if (prefs.getInt(Constants.BIZ_ID, 0) == 0) {
            performAsyncTask = new PerformAsyncTask(getActivity(), "InsertBizInfo",
                    Constants.INSERT_BIZ_INFO, postDataParams, "POST");
        } else {
            performAsyncTask = new PerformAsyncTask(getActivity(), "InsertBizInfo",
                    Constants.UPDATE_BIZ_INFO, postDataParams, "POST");
        }

        performAsyncTask.execute();
    }

    protected void postImageUpload(String response, boolean isSuccess) {
        if (!isSuccess) {
            progressBar.setVisibility(View.GONE);
            tick.setVisibility(View.VISIBLE);
            mainScroll.setVisibility(View.VISIBLE);
            Util.showSnackBar("Shop Object creation/updation failed. Pls Check connection and try again", mainScroll);
            return;
        }

        Gson gson = new Gson();

        JSONObject postDataParams = new JSONObject();

        Location location = Util.getLocation(getActivity());

        try {

            JSONArray shopCategories = new JSONArray();
            JSONObject men = new JSONObject();
            men.put("Men", gson.toJson(selectedMen));
            JSONObject women = new JSONObject();
            women.put("Women", gson.toJson(selectedWomen));
            shopCategories.put(men);
            shopCategories.put(women);

            if (prefs.getInt(Constants.BIZ_ID, 0) > 0) {
                postDataParams.put("bizId", prefs.getInt(Constants.BIZ_ID, 0));
            }


            gson = new Gson();
            String json = prefs.getString(Constants.BIZ_AREA_LIST, null);
            Type type = new TypeToken<List<String>>() {
            }.getType();
            ArrayList<String> shopAreaList = gson.fromJson(json, type);
            int shopAreaListPosition = prefs.getInt(Constants.SHOP_AREA, 0);

            postDataParams.put("gpsLatitude", location.getLatitude());
            postDataParams.put("gpsLongitude", location.getLongitude());
            postDataParams.put("bizName", prefs.getString(Constants.SHOP_NAME, "Shop Name"));
            postDataParams.put("bizStreetAddress", shopAreaList.get(shopAreaListPosition));
            postDataParams.put("bizMobile", prefs.getString(Constants.SHOP_PHONE, ""));
            if (!response.equals(""))
                postDataParams.put("bizProfile", response);
            postDataParams.put("bizCategory", shopCategories.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        PerformAsyncTask performAsyncTask;

        if (prefs.getInt(Constants.BIZ_ID, 0) == 0) {
            performAsyncTask = new PerformAsyncTask(getActivity(), "InsertBizInfo",
                    Constants.INSERT_BIZ_INFO, postDataParams, "POST");
        } else {
            performAsyncTask = new PerformAsyncTask(getActivity(), "InsertBizInfo",
                    Constants.UPDATE_BIZ_INFO, postDataParams, "POST");
        }

        performAsyncTask.execute();


    }

    protected void processServerResponse(String response, boolean isSuccess) {
        tick.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        mainScroll.setVisibility(View.GONE);

        if (!isSuccess) {
            progressBar.setVisibility(View.GONE);
            tick.setVisibility(View.VISIBLE);
            mainScroll.setVisibility(View.VISIBLE);
            Util.showSnackBar("Shop Object creation/updation failed. Pls Check connection and try again", mainScroll);
            return;
        }
        /*if (!response.equals("success"))
            ((MainActivity) getActivity()).createdBizId = Integer.parseInt(response);*/

        ((MainActivity) getActivity()).totalNumberOfCategoriesSelected = selectedMen.size() + selectedWomen.size();
        ((MainActivity) getActivity()).totalNumberOfCategoriesCreated = 0;

        if (((MainActivity) getActivity()).totalNumberOfCategoriesCreated == ((MainActivity) getActivity()).totalNumberOfCategoriesSelected) {
            Intent intent = new Intent(getActivity(), ShopDataViewPagerHost.class);
            startActivity(intent);
        }

        Gson gson = new Gson();
        String json = gson.toJson(selectedMen);
        prefs.edit().putString(Constants.SAVED_PREFS_MEN, json).apply();

        gson = new Gson();
        json = gson.toJson(selectedWomen);
        prefs.edit().putString(Constants.SAVED_PREFS_WOMEN, json).apply();

        for (int i = 0; i < selectedMen.size(); i++) {
            JSONArray linkedImages = new JSONArray();
            linkedImages.put("");
            linkedImages.put("");
            linkedImages.put("");

            JSONObject postDataParams = new JSONObject();
            try {
                postDataParams.put("bizId", prefs.getInt(Constants.BIZ_ID, 0));
                postDataParams.put("userCatLevel1", "Men");
                postDataParams.put("userCatLevel2", selectedMen.get(i));
                postDataParams.put("linkedImages", linkedImages);
                postDataParams.put("description", selectedMen.get(i));
                postDataParams.put("priceRangeStart", 0);
                postDataParams.put("priceRangeEnd", 1000);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "InsertRawCategoryObject",
                    Constants.INSERT_BIZ_CATEGORY, postDataParams, "POST");

            performAsyncTask.execute();
        }

        for (int i = 0; i < selectedWomen.size(); i++) {
            JSONArray linkedImages = new JSONArray();
            linkedImages.put("");
            linkedImages.put("");
            linkedImages.put("");

            JSONObject postDataParams = new JSONObject();
            try {
                postDataParams.put("bizId", prefs.getInt(Constants.BIZ_ID, 0));
                postDataParams.put("userCatLevel1", "Women");
                postDataParams.put("userCatLevel2", selectedWomen.get(i));
                postDataParams.put("linkedImages", linkedImages);
                postDataParams.put("description", selectedWomen.get(i));
                postDataParams.put("priceRangeStart", 0);
                postDataParams.put("priceRangeEnd", 1000);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "InsertRawCategoryObject",
                    Constants.INSERT_BIZ_CATEGORY, postDataParams, "POST");

            performAsyncTask.execute();
        }
    }

    CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            processSelectAll(buttonView, isChecked);
        }
    };

    @SuppressLint("WrongViewCast")
    protected void processSelectAll(CompoundButton button, boolean isChecked) {
        FlexboxLayout parentView;
        switch (button.getId()) {
            case R.id.select_all_men:
                parentView = (FlexboxLayout) ((LinearLayout) button.getParent().getParent()).findViewById(R.id.men_main);
                processSelectAllElements(selectedMen, isChecked, parentView);
                break;
            case R.id.select_all_women:
                parentView = (FlexboxLayout) ((LinearLayout) button.getParent().getParent()).findViewById(R.id.women_main);
                processSelectAllElements(selectedWomen, isChecked, parentView);
                break;
        }
    }

    protected void processSelectAllElements(ArrayList<String> checkList, boolean isChecked, FlexboxLayout parentView) {
        if (isChecked) {
            for (int i = 0; i < parentView.getChildCount(); i++) {
                String itemText = (String) ((TextView) ((LinearLayout) parentView.getChildAt(i)).findViewById(R.id.item1)).getText();
                if (!checkList.contains(itemText)) {
                    ((LinearLayout) parentView.getChildAt(i)).findViewById(R.id.item1).callOnClick();
                }
            }
        } else {
            for (int i = 0; i < parentView.getChildCount(); i++) {
                String itemText = (String) ((TextView) ((LinearLayout) parentView.getChildAt(i)).findViewById(R.id.item1)).getText();
                if (checkList.contains(itemText)) {
                    ((LinearLayout) parentView.getChildAt(i)).findViewById(R.id.item1).callOnClick();
                }
            }
        }
    }

    protected void saveUserPreferences() throws JSONException {

        if ((selectedMen.size() + selectedWomen.size()) < 5) {
            Util.showSnackBar("Please select at least 5", header);
            return;
        }


        JSONArray sample = new JSONArray();
        //---Setting Preferences ------

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());


        JSONArray filterCategories = new JSONArray();
        JSONObject subCategoryObject;


        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(selectedMen);
        editor.putString(Constants.USER_PREFERENCES_MEN, json);
        if (selectedMen.size() > 0) {
            sample.put("Men");
            subCategoryObject = new JSONObject();
            subCategoryObject.put("userCatLevel1", "Men");
            subCategoryObject.put("userCatLevel2", new JSONArray(json));
            filterCategories.put(subCategoryObject);
        }
        json = gson.toJson(selectedWomen);
        editor.putString(Constants.USER_PREFERENCES_WOMEN, json);
        if (selectedWomen.size() > 0) {
            sample.put("Women");
            subCategoryObject = new JSONObject();
            subCategoryObject.put("userCatLevel1", "Women");
            subCategoryObject.put("userCatLevel2", new JSONArray(json));
            filterCategories.put(subCategoryObject);
        }


        editor.apply();
    }

    protected void insertPreferences(FlexboxLayout parentView, int id, ArrayList<String> preferences, String savedKey) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        String json;
        json = prefs.getString(savedKey, null);
        JSONArray jsonArray = new JSONArray();
        ArrayList<String> selectedItems = new ArrayList<>();
        Gson gson = new Gson();
        if (json != null) {
            try {
                jsonArray = new JSONArray(json);
                Type type = new TypeToken<List<String>>() {
                }.getType();
                selectedItems = gson.fromJson(json, type);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < preferences.size(); i++) {

            String category = preferences.get(i);
            View view = (View) inflater.inflate(R.layout.single_item_textview, null);

            /*if (json != null) {
                if (selectedItems.contains(category)) {
                    ((TextView) view.findViewById(R.id.item1)).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selected_border_preference_tabs));
                    ((TextView) view.findViewById(R.id.item1)).setTextColor(getActivity().getResources().getColor(android.R.color.black));

                    if (id == R.id.men)
                        selectedMen.add(category);
                    if (id == R.id.women)
                        selectedWomen.add(category);
                }
            }*/

            TextView itemTextView1 = (TextView) view.findViewById(R.id.item1);
            if (alreadySelectedCats.contains(category)) {
                ((TextView) view.findViewById(R.id.item1)).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selected_border_preference_tabs));
                ((TextView) view.findViewById(R.id.item1)).setTextColor(getActivity().getResources().getColor(android.R.color.holo_red_dark));
            } else {
                itemTextView1.setOnClickListener(onClickListener);
            }

            itemTextView1.setText(category);
            parentView.addView(view);
        }
    }

    protected ArrayList<String> processSelectedItem(ArrayList<String> selectedPrefs, String choice, View v) {
        if (selectedPrefs.indexOf(choice) > -1) {
            selectedPrefs.remove(choice);
            ((TextView) v).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.unselected_border_preference_tabs));
            ((TextView) v).setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
        } else {
            selectedPrefs.add(choice);
            ((TextView) v).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selected_border_preference_tabs));
            ((TextView) v).setTextColor(getActivity().getResources().getColor(android.R.color.black));
        }
        return selectedPrefs;
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int selctedId = ((FlexboxLayout) v.getParent().getParent()).getId();

            switch (selctedId) {
                case R.id.men_main:
                    selectedMen = processSelectedItem(selectedMen, ((TextView) v).getText().toString(), v);
                    break;
                case R.id.women_main:
                    selectedWomen = processSelectedItem(selectedWomen, ((TextView) v).getText().toString(), v);
                    break;
            }
        }
    };
}
