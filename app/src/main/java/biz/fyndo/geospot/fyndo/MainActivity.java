package biz.fyndo.geospot.fyndo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nexmo.sdk.NexmoClient;
import com.nexmo.sdk.core.client.ClientBuilderException;
import com.nexmo.sdk.verify.client.VerifyClient;
import com.nexmo.sdk.verify.event.UserObject;
import com.nexmo.sdk.verify.event.VerifyClientListener;
import com.nexmo.sdk.verify.event.VerifyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    VerifyClient verifyClient;
    PhoneNumberFragment phoneNumberFragment;
    ProfileInfoFragment profileInfoFragment;
    SharedPreferences prefs;
    Context context;
    SubCategorySelectionFragment subCategorySelectionFragment;

    int totalNumberOfCategoriesSelected = 0;
    int totalNumberOfCategoriesCreated = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getApplicationContext();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("MainActivity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        context = this;
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        if (prefs.getBoolean(Constants.IS_LOGGED_IN, false)) {
            if (prefs.getInt(Constants.BIZ_ID, 0) == 0)
                launchProfileInfoFragment();
            else {
                Intent intent = new Intent(this, ShopDataViewPagerHost.class);
                startActivity(intent);
            }
            return;
        }
        instantiateNexmo();
        if (prefs.getBoolean(Constants.IS_FIRST_LAUNCH, true))
            launchSplashScreen();
        else
            launchPhoneNumberFragment();

    }

    protected void launchSplashScreen() {
        prefs.edit().putBoolean(Constants.IS_FIRST_LAUNCH, false).apply();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        SplashScreenFragment splashScreenFragment = new SplashScreenFragment();
        fragmentTransaction.add(R.id.main_content, splashScreenFragment);
        fragmentTransaction.commit();

    }

    protected void launchPhoneNumberFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        phoneNumberFragment = new PhoneNumberFragment();
        fragmentTransaction.add(R.id.main_content, phoneNumberFragment);
        fragmentTransaction.commit();
    }

    protected void launchProfileInfoFragment() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }
        });

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        profileInfoFragment = new ProfileInfoFragment(false);
        fragmentTransaction.replace(R.id.main_content, profileInfoFragment);
        fragmentTransaction.commit();
    }

    protected void launchSubCategorySelectorFragment() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }
        });

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        subCategorySelectionFragment = new SubCategorySelectionFragment();
        fragmentTransaction.replace(R.id.main_content, subCategorySelectionFragment);
        fragmentTransaction.commit();
    }

    protected void instantiateNexmo() {
        Context context = getApplicationContext();
        NexmoClient nexmoClient = null;
        try {
            nexmoClient = new NexmoClient.NexmoClientBuilder()
                    .context(context)
                    .applicationId(Constants.NEXMO_APP_ID) //your App key
                    .sharedSecretKey(Constants.NEXMO_SHARED_SECRET) //your App secret
                    .build();
        } catch (ClientBuilderException e) {
            e.printStackTrace();
        }

        verifyClient = new VerifyClient(nexmoClient);
        verifyClient.addVerifyListener(verifyClientListener);
    }

    protected void postUserCreated(String response, boolean isSuccess) {

        if (!isSuccess) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    phoneNumberFragment.populatePhoneNumberView();
                }
            });
            Util.showSnackBar("Something went wrong. Please try again.", phoneNumberFragment.actionButton);
            return;
        }

        prefs.edit().putString(Constants.USER_ID, response).apply();

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "GetBizAreas",
                Constants.SELECT_BIZ_AREAS, null, "GET");

        performAsyncTask.execute();
    }

    protected void checkIfUserExists(String response, boolean isSuccess) {

        if (!isSuccess) {
            phoneNumberFragment.createNewUser();
            return;
        }

        Gson gson = new Gson();
        String json = prefs.getString(Constants.BIZ_AREA_LIST, null);
        Type type = new TypeToken<List<String>>() {
        }.getType();
        ArrayList<String> shopAreaList = gson.fromJson(json, type);

        String userID = "";
        try {
            JSONObject responseObject = new JSONObject(response);
            userID = responseObject.getJSONObject("user").getString("userId");
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(Constants.USER_ID, userID);
            editor.putString(Constants.USER_NAME, responseObject.getJSONObject("user").getString("userName"));
            editor.putString(Constants.USER_EMAIL, responseObject.getJSONObject("user").getString("emailId"));
            editor.putString(Constants.USER_PHONE, responseObject.getJSONObject("user").getString("mobilePh"));
            editor.putString(Constants.USER_PROFILE_IMAGE, responseObject.getJSONObject("user").getString("userProfile"));

            if (!responseObject.isNull("bizInfo")) {
                editor.putInt(Constants.BIZ_ID, responseObject.getJSONObject("bizInfo").getInt("bizId"));
                editor.putString(Constants.SHOP_NAME, responseObject.getJSONObject("bizInfo").getString("bizName"));
                editor.putString(Constants.SHOP_DESCRIPTION, responseObject.getJSONObject("bizInfo").getString("bizDesc"));
                editor.putString(Constants.SHOP_PHONE, responseObject.getJSONObject("bizInfo").getString("bizMobile"));
                editor.putString(Constants.SHOP_PROFILE_IMAGE, responseObject.getJSONObject("bizInfo").getString("bizProfile"));
                editor.putString(Constants.SHOP_CATEGORY_SERVER, responseObject.getJSONObject("bizInfo").getString("bizCategory"));
                editor.putString(Constants.SHOP_AREA_SERVER, responseObject.getJSONObject("bizInfo").getString("bizArea"));

                String bizType = "";
                bizType = responseObject.getJSONObject("bizInfo").getString("bizType");
                if (bizType.equals("Eatery"))
                    editor.putInt(Constants.SHOP_TYPE, 0);
                else
                    editor.putInt(Constants.SHOP_TYPE, 1);

                String relationToShop = "";
                relationToShop = responseObject.getJSONObject("bizInfo").getString("bizType");
                if (relationToShop.equals("Owner"))
                    editor.putInt(Constants.USER_RELATION_TO_STORE, 0);
                else
                    editor.putInt(Constants.USER_RELATION_TO_STORE, 1);

            }
            editor.apply();


        } catch (JSONException e) {
            e.printStackTrace();
        }

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "GetBizAreas",
                Constants.SELECT_BIZ_AREAS, null, "GET");

        performAsyncTask.execute();
    }

    protected void saveBizAreas(String response, boolean isSuccess) {
        if (!isSuccess) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    phoneNumberFragment.populatePhoneNumberView();
                }
            });
            Util.showSnackBar("Something went wrong. Please try again.", phoneNumberFragment.actionButton);
            return;
        }

        ArrayList<String> shopAreas = new ArrayList<>();
        JSONArray responseArray = null;
        try {
            responseArray = new JSONArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < responseArray.length(); i++) {
            JSONObject jsonObject = null;
            try {
                jsonObject = responseArray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (jsonObject == null)
                continue;

            try {
                shopAreas.add(jsonObject.getString("areaName"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(shopAreas);
        editor.putString(Constants.BIZ_AREA_LIST, json);
        editor.apply();

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "GetBizCategories",
                Constants.SELECT_BIZ_CATEGORIES, null, "GET");

        performAsyncTask.execute();


    }

    protected void processRawSubcatCreation() {

        ++totalNumberOfCategoriesCreated;

        if (totalNumberOfCategoriesCreated == totalNumberOfCategoriesSelected) {
            Intent intent = new Intent(context, ShopDataViewPagerHost.class);
            context.startActivity(intent);
        }

    }

    protected void saveBizCategories(String response, boolean isSuccess) {
        if (!isSuccess) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    phoneNumberFragment.populatePhoneNumberView();
                }
            });
            Util.showSnackBar("Something went wrong. Please try again.", phoneNumberFragment.actionButton);
            return;
        }

        ArrayList<String> shopCategoryStore = new ArrayList<>();
        ArrayList<String> shopCategoryEatery = new ArrayList<>();
        JSONArray responseArray = null;
        try {
            responseArray = new JSONArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String type;

        for (int i = 0; i < responseArray.length(); i++) {
            JSONObject jsonObject = null;
            try {
                jsonObject = responseArray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (jsonObject == null)
                continue;

            try {
                type = jsonObject.getString("bizType");
                if (type.equals("Eatery")) {
                    shopCategoryEatery.add(jsonObject.getString("bizCategory"));
                } else {
                    shopCategoryStore.add(jsonObject.getString("bizCategory"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(shopCategoryStore);
        editor.putString(Constants.SHOP_CATEGORY_STORE, json);
        json = gson.toJson(shopCategoryEatery);
        editor.putString(Constants.SHOP_CATEGORY_EATERY, json);
        editor.apply();

        editor.putBoolean(Constants.IS_LOGGED_IN, true);
        editor.apply();

        if (prefs.getInt(Constants.BIZ_ID, 0) == 0)
            launchProfileInfoFragment();
        else {
            /*Intent intent = new Intent(this, ShopDataViewPagerHost.class);
            startActivity(intent);*/
            launchSubCategorySelectorFragment();
        }

    }

    protected void postUserBizInfoUpdated(String response, boolean isSuccess) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                profileInfoFragment.progressBar.setVisibility(View.GONE);
                profileInfoFragment.actionButton.setVisibility(View.VISIBLE);
            }
        });

        if (!isSuccess) {
            Util.showSnackBar("Something went wrong. Please try again.", profileInfoFragment.shopPhone);
            return;
        }

        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(Constants.BIZ_ID, Integer.parseInt(response));
        editor.apply();


        Intent intent = new Intent(this, ShopDataViewPagerHost.class);
        startActivity(intent);
    }

    VerifyClientListener verifyClientListener = new VerifyClientListener() {
        @Override
        public void onVerifyInProgress(VerifyClient verifyClient, UserObject user) {

        }

        @Override
        public void onUserVerified(VerifyClient verifyClient, UserObject user) {

            /*JSONObject postDataParams = new JSONObject();
            try {
                postDataParams.put("mobilePh", prefs.getString(Constants.USER_PHONE, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "MainActivity",
                    Constants.INSERT_USER, postDataParams, "POST");

            performAsyncTask.execute();*/

            PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "CheckPhoneNumber",
                    Constants.SELECT_USER_BY_PHONE + prefs.getString(Constants.USER_PHONE, ""), null, "GET");

            performAsyncTask.execute();
        }

        @Override
        public void onError(VerifyClient verifyClient, VerifyError errorCode, UserObject user) {
            if (phoneNumberFragment != null) {
                phoneNumberFragment.otpVerificationError();
            }
        }

        @Override
        public void onException(IOException exception) {
            if (phoneNumberFragment != null) {
                phoneNumberFragment.otpVerificationError();
            }
        }
    };

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
