package biz.fyndo.geospot.fyndo;


import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;

import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by vibinvarghese on 28/08/16.
 */
public class PhoneNumberFragment extends Fragment {

    ProgressBar progressBar;
    Button actionButton;
    TextInputLayout numberInputLayout;
    TextView changePhoneNumber, description;
    EditText numberInputEditText;
    SharedPreferences prefs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.phone_number_fragment, container, false);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        final SharedPreferences.Editor editor = prefs.edit();

        description = (TextView) rootView.findViewById(R.id.description);
        changePhoneNumber = (TextView) rootView.findViewById(R.id.change_phone_number);
        numberInputLayout = (TextInputLayout) rootView.findViewById(R.id.number_text_input);
        numberInputEditText = (EditText) rootView.findViewById(R.id.number);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress);
        actionButton = (Button) rootView.findViewById(R.id.button_action);
        TextView logo = (TextView) rootView.findViewById(R.id.logo_text);

        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/museo.ttf");
        logo.setTypeface(face);

        numberInputLayout.requestFocus();

        progressBar.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (actionButton.getText().equals("Send OTP")) {
                    prefs.edit().putString(Constants.USER_PHONE, numberInputEditText.getText().toString()).apply();

                    PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "CheckPhoneNumber",
                            Constants.SELECT_USER_BY_PHONE + prefs.getString(Constants.USER_PHONE, ""), null, "GET");


                    performAsyncTask.execute();


                    actionButton.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);


                }


                    /*if (numberInputEditText.getText().length() != 10) {
                        Util.showSnackBar("Please Enter a valid mobile number",
                                numberInputLayout);
                        return;
                    }

                    *//*actionButton.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);*//*

                    editor.putString(Constants.USER_PHONE, numberInputEditText.getText().toString());
                    editor.apply();


                    *//*PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "CheckPhoneNumber",
                            Constants.SELECT_USER_BY_PHONE + numberInputEditText.getText().toString(), null, "GET");

                    performAsyncTask.execute();

                    return;*//*

                    actionButton.setText("SUBMIT");
                    description.setText("Enter OTP");
                    numberInputLayout.setHint("Enter OTP");
                    changePhoneNumber.setVisibility(View.VISIBLE);
                    numberInputLayout.setCounterMaxLength(4);
                    InputFilter[] FilterArray = new InputFilter[1];
                    FilterArray[0] = new InputFilter.LengthFilter(4);
                    numberInputEditText.setFilters(FilterArray);

                    editor.putString(Constants.USER_PHONE, numberInputEditText.getText().toString());
                    editor.apply();

                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.toggleSoftInputFromWindow(numberInputEditText.getApplicationWindowToken(), InputMethodManager.RESULT_SHOWN, 0);


                    //Nexmo Verification
                    ((MainActivity) getActivity()).verifyClient.getVerifiedUser("IN",
                            numberInputEditText.getText().toString());

                    numberInputEditText.setText("");

                } else {
                    numberInputLayout.setEnabled(false);
                    progressBar.setVisibility(View.VISIBLE);
                    actionButton.setVisibility(View.GONE);
                    ((MainActivity) getActivity()).verifyClient.checkPinCode(
                            numberInputEditText.getText().toString());
                }*/
            }
        });

        changePhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                populatePhoneNumberView();
            }
        });

        return rootView;
    }

    protected void createNewUser() {
        JSONObject postDataParams = new JSONObject();
        try {
            postDataParams.put("mobilePh", prefs.getString(Constants.USER_PHONE, ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "MainActivity",
                Constants.INSERT_USER, postDataParams, "POST");

        performAsyncTask.execute();
    }

    protected void populatePhoneNumberView() {
        actionButton.setText("Send OTP");
        actionButton.setVisibility(View.VISIBLE);
        changePhoneNumber.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        numberInputLayout.setEnabled(true);
        numberInputEditText.setText("");
        numberInputLayout.setHint("Enter Number");
        description.setText(R.string.enter_mobile_number_to_get_started);
        numberInputLayout.setCounterMaxLength(10);
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(10);
        numberInputEditText.setFilters(FilterArray);
    }

    protected void otpVerificationError() {
        Util.showSnackBar("OTP verification failed", numberInputLayout);
        progressBar.setVisibility(View.GONE);
        actionButton.setVisibility(View.VISIBLE);
        numberInputLayout.setEnabled(true);
    }
}
