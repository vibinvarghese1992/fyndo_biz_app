package biz.fyndo.geospot.fyndo;

import android.*;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by vibinvarghese on 29/08/16.
 */
public class ProfileInfoFragment extends Fragment {

    boolean isUpdateInfo;
    EditText shopName, shopDescription, shopPhone, userName, userEmail, userPhone;
    Spinner shopType, shopArea, shopCategory, userRelationToStore;
    ProgressBar progressBar;
    Button actionButton;
    SharedPreferences prefs;
    ImageView profileImage, shopImage, selectedImage;
    ArrayAdapter<CharSequence> adapter;
    boolean isFirstLaunch = true, profileImageSet = false, shopImageSet = false;
    String userChoosenTask;
    public final int REQUEST_CAMERA = 111;
    public final int SELECT_FILE = 112;
    public final int CROPED_SELECT_FILE = 113;
    public final int CROPED_REQUEST_CAMERA = 114;
    Bitmap userImageBitampCaptured, shopImageBitampCaptured;
    Uri capturedImageUri;
    Intent storedData = null;

    ArrayList<String> shopAreaList;
    ArrayList<String> shopCategoryStore;
    ArrayList<String> shopCategoryEatery;

    public ProfileInfoFragment() {
    }

    @SuppressLint("ValidFragment")
    public ProfileInfoFragment(boolean isUpdateInfo) {
        this.isUpdateInfo = isUpdateInfo;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.profile_fragment, container, false);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getContext().getApplicationContext();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("ProfileFragment");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        checkForStoragePermission();


        initUI(rootView);

        shopName.requestFocus();
        actionButton = (Button) rootView.findViewById(R.id.button_action);

        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(actionButton.getApplicationWindowToken(), InputMethodManager.RESULT_HIDDEN, 0);

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        if (isUpdateInfo) {
            actionButton.setText("Update");
        }

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveProfileInfo();
            }
        });

        return rootView;
    }

    protected void checkForStoragePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return;

        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION}, 111);
    }

    private void cropImage(Intent data, int requestCode) {
        Bitmap bitmap = null;
        if (requestCode == CROPED_SELECT_FILE) {
            if (data != null) {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(
                        getActivity().getContentResolver(), capturedImageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            cropIntent.setDataAndType(Util.getImageUri(getActivity(), bitmap), "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 300);
            cropIntent.putExtra("outputY", 300);
            cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, requestCode);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            if (requestCode == CROPED_SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == CROPED_REQUEST_CAMERA)
                onCaptureImageResult(data);
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    protected void saveProfileInfo() {
        SharedPreferences.Editor editor = prefs.edit();

        if (shopName.getText().length() == 0) {
            Util.showSnackBar("Please Enter Shop Name", shopName);
            return;
        } else if (shopDescription.getText().length() == 0) {
            Util.showSnackBar("Please Enter Shop Description", shopDescription);
            return;
        } else if (shopPhone.getText().length() == 0) {
            Util.showSnackBar("Please Enter Shop Phone", shopPhone);
            return;
        } else if (userName.getText().length() == 0) {
            Util.showSnackBar("Please Enter Full Name", shopDescription);
            return;
        } else if (userPhone.getText().length() == 0) {
            Util.showSnackBar("Please Enter Mobile Number", shopDescription);
            return;
        }

        editor.putString(Constants.SHOP_NAME, shopName.getText().toString());
        editor.putString(Constants.SHOP_DESCRIPTION, shopDescription.getText().toString());
        editor.putString(Constants.SHOP_PHONE, shopPhone.getText().toString());
        editor.putString(Constants.USER_NAME, userName.getText().toString());
        editor.putString(Constants.USER_EMAIL, userEmail.getText().toString());
        editor.putString(Constants.USER_PHONE, userPhone.getText().toString());

        editor.putInt(Constants.SHOP_AREA, shopArea.getSelectedItemPosition());
        editor.putInt(Constants.SHOP_TYPE, shopType.getSelectedItemPosition());
        editor.putInt(Constants.SHOP_CATEGORY, shopCategory.getSelectedItemPosition());
        editor.putInt(Constants.USER_RELATION_TO_STORE, userRelationToStore.getSelectedItemPosition());

        editor.putBoolean(Constants.IS_PROFILE_FILLED, true);
        editor.apply();


        progressBar.setVisibility(View.VISIBLE);
        actionButton.setVisibility(View.GONE);

        uploadProfileImage();

    }

    protected void uploadProfileImage() {
        if (profileImageSet) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            userImageBitampCaptured.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArray = baos.toByteArray();

            PerformImageUpload performImageUpload = new PerformImageUpload(getActivity(), byteArray, null, this, "profile_image.jpg");
            performImageUpload.execute();
        } else {
            uploadShopImage();
        }
    }

    protected void uploadShopImage() {
        if (shopImageSet) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            shopImageBitampCaptured.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArray = baos.toByteArray();

            PerformImageUpload performImageUpload = new PerformImageUpload(getActivity(), byteArray, null, this, "shop_image.jpg");
            performImageUpload.execute();
        } else {
            uploadProfileDataToServer();
        }
    }

    protected void uploadProfileDataToServer() {
        JSONObject postDataParams = new JSONObject();
        JSONObject userData = new JSONObject();
        JSONObject bizInfoData = new JSONObject();
        try {

            Location latlng = Util.getLocation(getActivity());
            if (isUpdateInfo && prefs.getInt(Constants.BIZ_ID, 0) != 0) {
                isUpdateInfo = true;
                userData.put("userId", prefs.getString(Constants.USER_ID, ""));
                userData.put("userName", userName.getText());
                userData.put("emailId", userEmail.getText());
                userData.put("mobilePh", userPhone.getText());
                userData.put("userProfile", prefs.getString(Constants.USER_PROFILE_IMAGE, ""));

                bizInfoData.put("bizName", shopName.getText());
                bizInfoData.put("bizDesc", shopDescription.getText());
                bizInfoData.put("bizType", shopType.getSelectedItem().toString());
                bizInfoData.put("bizArea", shopArea.getSelectedItem().toString());
                bizInfoData.put("bizMobile", shopPhone.getText());
                bizInfoData.put("gpsLatitude", latlng.getLatitude());
                bizInfoData.put("gpsLongitude", latlng.getLongitude());
                //bizInfoData.put("bizCategory", shopCategory.getSelectedItem().toString());
                bizInfoData.put("userRelation", userRelationToStore.getSelectedItem().toString());
                bizInfoData.put("bizProfile", prefs.getString(Constants.SHOP_PROFILE_IMAGE, ""));
                bizInfoData.put("bizId", prefs.getInt(Constants.BIZ_ID, 0));

                postDataParams.put("user", userData);
                postDataParams.put("bizInfo", bizInfoData);
            } else {
                isUpdateInfo = false;
                postDataParams.put("bizName", shopName.getText());
                postDataParams.put("bizDesc", shopDescription.getText());
                postDataParams.put("bizType", shopType.getSelectedItem().toString());
                postDataParams.put("bizArea", shopArea.getSelectedItem().toString());
                postDataParams.put("bizMobile", shopPhone.getText());
                postDataParams.put("gpsLatitude", latlng.getLatitude());
                postDataParams.put("gpsLongitude", latlng.getLongitude());
                //postDataParams.put("bizCategory", shopCategory.getSelectedItem().toString());
                postDataParams.put("userRelation", userRelationToStore.getSelectedItem().toString());
                postDataParams.put("bizProfile", prefs.getString(Constants.SHOP_PROFILE_IMAGE, ""));
                postDataParams.put("fyndoUserId", prefs.getString(Constants.USER_ID, ""));
                postDataParams.put("bizClaimDate", String.valueOf(System.currentTimeMillis()));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        PerformAsyncTask performAsyncTask;
        if (!isUpdateInfo) {
            performAsyncTask = new PerformAsyncTask(getActivity(), "InsertProfileInfoFragment",
                    Constants.INSERT_BIZ_INFO, postDataParams, "POST");
        } else {
            performAsyncTask = new PerformAsyncTask(getActivity(), "UpdateProfileInfoFragment",
                    Constants.UPDATE_BIZ_INFO, postDataParams, "POST");
        }

        performAsyncTask.execute();
    }

    protected void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                dialog.dismiss();
                boolean result = Util.checkPermission(getActivity());
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        capturedImageUri = getActivity().getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (data != null && data.getData() != null)
                storedData = data;
            if (requestCode == SELECT_FILE)
                cropImage(data, CROPED_SELECT_FILE);
            else if (requestCode == REQUEST_CAMERA)
                cropImage(data, CROPED_REQUEST_CAMERA);
            else if (requestCode == CROPED_SELECT_FILE)
                onSelectFromGalleryResult(storedData);
            else if (requestCode == CROPED_REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bm, "loadImage", null);

        Picasso.with(getActivity()).load(path)
                .resize(120, 120)
                .centerCrop()
                .into(selectedImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        Bitmap imageBitmap = ((BitmapDrawable) selectedImage.getDrawable()).getBitmap();
                        RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
                        imageDrawable.setCircular(true);
                        imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getWidth()) / 2.0f);
                        selectedImage.setImageDrawable(imageDrawable);
                    }

                    @Override
                    public void onError() {
                        selectedImage.setImageResource(R.drawable.store);
                    }
                });

        selectedImage.setPadding(0, 0, 0, 0);

        if (selectedImage.equals(profileImage)) {
            profileImageSet = true;
            userImageBitampCaptured = bm;
        }
        if (selectedImage.equals(shopImage)) {
            shopImageSet = true;
            shopImageBitampCaptured = bm;
        }

    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = null;
        try {
            thumbnail = MediaStore.Images.Media.getBitmap(
                    getActivity().getContentResolver(), capturedImageUri);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".png");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Picasso.with(getActivity()).load(destination)
                .resize(120, 120)
                .centerCrop()
                .into(selectedImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        Bitmap imageBitmap = ((BitmapDrawable) selectedImage.getDrawable()).getBitmap();
                        RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
                        imageDrawable.setCircular(true);
                        imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getWidth()) / 2.0f);
                        selectedImage.setImageDrawable(imageDrawable);
                    }

                    @Override
                    public void onError() {
                        selectedImage.setImageResource(R.drawable.store);
                    }
                });

        selectedImage.setPadding(0, 0, 0, 0);

        if (selectedImage.equals(profileImage)) {
            profileImageSet = true;
            userImageBitampCaptured = thumbnail;
        }
        if (selectedImage.equals(shopImage)) {
            shopImageSet = true;
            shopImageBitampCaptured = thumbnail;
        }
    }


    protected void initUI(View rootView) {

        shopType = (Spinner) rootView.findViewById(R.id.shop_type);
        ArrayAdapter<CharSequence> shopTypeAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.shop_type, android.R.layout.simple_spinner_item);
        shopTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        shopType.setAdapter(shopTypeAdapter);

        Gson gson = new Gson();
        String json = prefs.getString(Constants.BIZ_AREA_LIST, null);
        Type type = new TypeToken<List<String>>() {
        }.getType();
        shopAreaList = gson.fromJson(json, type);

        gson = new Gson();
        json = prefs.getString(Constants.SHOP_CATEGORY_EATERY, null);
        final ArrayList<String> shopCategoryEatery = gson.fromJson(json, type);

        gson = new Gson();
        json = prefs.getString(Constants.SHOP_CATEGORY_STORE, null);
        final ArrayList<String> shopCategoryStore = gson.fromJson(json, type);

        shopArea = (Spinner) rootView.findViewById(R.id.shop_area);
        ArrayAdapter shopAreaAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, shopAreaList);
        shopAreaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        shopArea.setAdapter(shopAreaAdapter);

        userRelationToStore = (Spinner) rootView.findViewById(R.id.relation_to_store);
        ArrayAdapter<CharSequence> userRelationAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.relation_items, android.R.layout.simple_spinner_item);
        userRelationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        userRelationToStore.setAdapter(userRelationAdapter);

        shopCategory = (Spinner) rootView.findViewById(R.id.shop_category);
        adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.shop_category_eatery, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        shopCategory.setAdapter(adapter);

        shopType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (isFirstLaunch) {
                    isFirstLaunch = false;
                    return;
                }
                callBackToPopulateShopCategory(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        shopName = (EditText) rootView.findViewById(R.id.shop_name);
        shopDescription = (EditText) rootView.findViewById(R.id.shop_description);
        shopPhone = (EditText) rootView.findViewById(R.id.shop_phone);
        userName = (EditText) rootView.findViewById(R.id.full_name);
        userEmail = (EditText) rootView.findViewById(R.id.email);
        userPhone = (EditText) rootView.findViewById(R.id.mobile);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress);
        profileImage = (ImageView) rootView.findViewById(R.id.profile_icon_image);
        shopImage = (ImageView) rootView.findViewById(R.id.shop_icon_image);

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedImage = profileImage;
                selectImage();
            }
        });

        shopImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedImage = shopImage;
                selectImage();
            }
        });

        /*if (prefs.getBoolean(Constants.IS_PROFILE_FILLED, false)) {
            prefillData();
        }*/

        prefillData();

        userPhone.setText(prefs.getString(Constants.USER_PHONE, ""));
    }

    protected void callBackToPopulateShopCategory(int position) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<String>>() {
        }.getType();
        String json = prefs.getString(Constants.SHOP_CATEGORY_EATERY, null);
        shopCategoryEatery = gson.fromJson(json, type);

        gson = new Gson();
        json = prefs.getString(Constants.SHOP_CATEGORY_STORE, null);
        shopCategoryStore = gson.fromJson(json, type);

        if (position == 0) {
            adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, shopCategoryEatery);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            shopCategory.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        } else {
            adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, shopCategoryStore);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            shopCategory.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        }
    }

    protected void populateServerDataForSpinners() {
        String shopAreaServer = prefs.getString(Constants.SHOP_AREA_SERVER, "");
        shopArea.setSelection(shopAreaList.indexOf(shopAreaServer));
        prefs.edit().putString(Constants.SHOP_AREA_SERVER, "").apply();
        if (shopAreaList.indexOf(shopAreaServer) < 0)
            prefs.edit().putInt(Constants.SHOP_AREA, 0).apply();
        else
            prefs.edit().putInt(Constants.SHOP_AREA, shopAreaList.indexOf(shopAreaServer)).apply();

        int shopType = prefs.getInt(Constants.SHOP_TYPE, 0);

        int shopCategoryPosition = 0;
        if (shopType == 0) {
            shopCategoryPosition = shopCategoryEatery.indexOf(prefs.getString(Constants.SHOP_CATEGORY_SERVER, ""));
        } else {
            shopCategoryPosition = shopCategoryStore.indexOf(prefs.getString(Constants.SHOP_CATEGORY_SERVER, ""));
        }

        shopCategory.setSelection(shopCategoryPosition);

        prefs.edit().putString(Constants.SHOP_CATEGORY_SERVER, "").apply();
        prefs.edit().putInt(Constants.SHOP_CATEGORY, shopCategoryPosition).apply();
    }

    protected void prefillData() {

        shopType.setSelection(prefs.getInt(Constants.SHOP_TYPE, 0));
        shopArea.setSelection(prefs.getInt(Constants.SHOP_AREA, 0));
        userRelationToStore.setSelection(prefs.getInt(Constants.USER_RELATION_TO_STORE, 0));
        shopName.setText(prefs.getString(Constants.SHOP_NAME, ""));
        shopDescription.setText(prefs.getString(Constants.SHOP_DESCRIPTION, ""));
        shopPhone.setText(prefs.getString(Constants.SHOP_PHONE, ""));
        String name = prefs.getString(Constants.USER_NAME, "");
        if (!name.equals("null"))
            userName.setText(prefs.getString(Constants.USER_NAME, ""));
        String email = prefs.getString(Constants.USER_EMAIL, "");
        if (!email.equals("null"))
            userEmail.setText(prefs.getString(Constants.USER_EMAIL, ""));
        userPhone.setText(prefs.getString(Constants.USER_PHONE, ""));
        callBackToPopulateShopCategory(prefs.getInt(Constants.SHOP_TYPE, 0));
        shopCategory.setSelection(prefs.getInt(Constants.SHOP_CATEGORY, 0));

        if (!prefs.getString(Constants.SHOP_AREA_SERVER, "").equals("")) {
            populateServerDataForSpinners();
        }

        String shopImagePath = prefs.getString(Constants.SHOP_PROFILE_IMAGE, "");
        String userImagePath = prefs.getString(Constants.USER_PROFILE_IMAGE, "");
        if (!userImagePath.equals("") && !userImagePath.equals("null")) {
            Picasso.with(getActivity()).load(Constants.ROOT_URL + userImagePath)
                    .resize(120, 120)
                    .placeholder(R.drawable.account_place_holder)
                    .into(profileImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) profileImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getWidth()) / 2.0f);
                            profileImage.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError() {
                            profileImage.setImageResource(R.drawable.account);
                        }
                    });
            profileImage.setPadding(0, 0, 0, 0);
        }

        if (!shopImagePath.equals("") && !shopImagePath.equals("null")) {
            Picasso.with(getActivity()).load(Constants.ROOT_URL + shopImagePath)
                    .resize(120, 120)
                    .placeholder(R.drawable.store_place_holder)
                    .into(shopImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) shopImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getWidth()) / 2.0f);
                            shopImage.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError() {
                            shopImage.setImageResource(R.drawable.store);
                        }
                    });
            shopImage.setPadding(0, 0, 0, 0);
        }

    }
}
