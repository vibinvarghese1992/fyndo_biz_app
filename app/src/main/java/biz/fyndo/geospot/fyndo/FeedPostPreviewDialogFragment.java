package biz.fyndo.geospot.fyndo;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by vibinvarghese on 25/09/16.
 */

public class FeedPostPreviewDialogFragment extends DialogFragment implements ViewPager.OnPageChangeListener {

    PostFragment postFragment;
    CustomViewPager viewPager;
    LinearLayout pager_indicator;
    ViewPagerAdapter mAdapter;
    private int dotsCount;
    private ImageView[] dots;
    Context context;
    boolean hideProgress = true;

    public FeedPostPreviewDialogFragment() {
    }

    @SuppressLint("ValidFragment")
    public FeedPostPreviewDialogFragment(Context context, PostFragment postFragment) {
        this.postFragment = postFragment;
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.post_preview_dialog, container, false);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        TextView title = (TextView) rootView.findViewById(R.id.title);
        TextView description = (TextView) rootView.findViewById(R.id.description);
        TextView validity = (TextView) rootView.findViewById(R.id.validity);
        TextView edit = (TextView) rootView.findViewById(R.id.edit);
        final Button post = (Button) rootView.findViewById(R.id.post);
        TextView shopName = (TextView) rootView.findViewById(R.id.shop_name);
        TextView shopArea = (TextView) rootView.findViewById(R.id.shop_area);
        viewPager = (CustomViewPager) rootView.findViewById(R.id.pager_introduction);
        pager_indicator = (LinearLayout) rootView.findViewById(R.id.viewPagerCountDots);
        final ImageView shopImage = (ImageView) rootView.findViewById(R.id.shop_icon);

        ArrayList<Bitmap> images = new ArrayList<>();

        if (postFragment.imageBitmap1 != null) {
            images.add(postFragment.imageBitmap1);
        }
        if (postFragment.imageBitmap2 != null) {
            images.add(postFragment.imageBitmap2);
        }
        if (postFragment.imageBitmap3 != null) {
            images.add(postFragment.imageBitmap3);
        }


        mAdapter = new ViewPagerAdapter(getActivity(), images);
        viewPager.setAdapter(mAdapter);
        viewPager.setCurrentItem(0);
        viewPager.setOnPageChangeListener(this);
        if (mAdapter.getCount() > 1)
            setUiPageViewController();

        /*shopArea.setText(postFragment.shopArea.getText());
        shopName.setText(postFragment.shopName.getText());
        title.setText(postFragment.postTitle.getText());
        description.setText(postFragment.content.getText());
        validity.setText(postFragment.duration.getText().toString().split(" ")[2] + " day(s) left");*/

        String shopImagePath = prefs.getString(Constants.SHOP_PROFILE_IMAGE, "");

        if (!shopImagePath.equals("")) {
            Picasso.with(getActivity()).load(Constants.ROOT_URL + shopImagePath)
                    .resize(48, 48)
                    .placeholder(R.drawable.store_place_holder)
                    .into(shopImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            if (isAdded()) {
                                Bitmap imageBitmap = ((BitmapDrawable) shopImage.getDrawable()).getBitmap();
                                RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
                                imageDrawable.setCircular(true);
                                imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getWidth()) / 2.0f);
                                shopImage.setImageDrawable(imageDrawable);
                            }
                        }

                        @Override
                        public void onError() {
                            if (isAdded())
                                shopImage.setImageResource(R.drawable.store);
                        }
                    });
            shopImage.setPadding(4, 4, 4, 4);
        }

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postFragment.progressBar.setVisibility(View.GONE);
                postFragment.postButton.setVisibility(View.VISIBLE);
                dismiss();
            }
        });

        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideProgress = false;
                dismiss();
                postFragment.postImagesToServer();
            }
        });

        return rootView;
    }

    private void setUiPageViewController() {

        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];
        pager_indicator.removeAllViews();

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mAdapter.getCount() > 1) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
            }

            dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
        }

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state != ViewPager.SCROLL_STATE_IDLE) {
            final int childCount = viewPager.getChildCount();
            for (int i = 0; i < childCount; i++)
                viewPager.getChildAt(i).setLayerType(View.LAYER_TYPE_NONE, null);
        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (hideProgress) {
            postFragment.progressBar.setVisibility(View.GONE);
            postFragment.postButton.setVisibility(View.VISIBLE);
        }
    }
}

class ViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<Bitmap> mResources;

    public ViewPagerAdapter(Context mContext, ArrayList<Bitmap> mResources) {
        this.mContext = mContext;
        this.mResources = mResources;
    }

    @Override
    public int getCount() {
        return mResources.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.img_pager_item);

        Picasso.with(mContext)
                .load(getImageUri(mContext, mResources.get(position)))
                .resize(300, 300)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .error(R.drawable.image_place_holder)
                .into(imageView);
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
