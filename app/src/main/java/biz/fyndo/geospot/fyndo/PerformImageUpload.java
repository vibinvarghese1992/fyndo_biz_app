package biz.fyndo.geospot.fyndo;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by vibinvarghese on 17/09/16.
 */

public class PerformImageUpload extends AsyncTask {

    Context context;
    byte image[];
    PostFragment postFragment;
    ProfileInfoFragment profileInfoFragment;
    String fileName;

    public PerformImageUpload(Context context, byte image[], @Nullable PostFragment postFragment, @Nullable ProfileInfoFragment profileInfoFragment, String fileName) {
        this.image = image;
        this.postFragment = postFragment;
        this.fileName = fileName;
        this.context = context;
        this.profileInfoFragment = profileInfoFragment;
    }

    @Override
    protected Object doInBackground(Object[] params) {


        try {
            URL url = new URL(Constants.SAVE_FILE);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");

            MultipartEntity entity = new MultipartEntity(
                    HttpMultipartMode.BROWSER_COMPATIBLE);

            ByteArrayBody bab = new ByteArrayBody(image, fileName);

            entity.addPart("file", bab);

            conn.addRequestProperty("Content-length", entity.getContentLength() + "");
            conn.addRequestProperty(entity.getContentType().getName(), entity.getContentType().getValue());

            OutputStream os = conn.getOutputStream();
            entity.writeTo(conn.getOutputStream());
            os.close();
            conn.connect();


            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                String response = "";
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
                if (postFragment != null)
                    postFragment.postNextImageToServer(response);
                if (profileInfoFragment != null) {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                    SharedPreferences.Editor editor = prefs.edit();
                    if (fileName.equals("profile_image.jpg")) {
                        editor.putString(Constants.USER_PROFILE_IMAGE, response);
                        editor.apply();
                        profileInfoFragment.uploadShopImage();
                    } else {
                        editor.putString(Constants.SHOP_PROFILE_IMAGE, response);
                        editor.apply();
                        profileInfoFragment.uploadProfileDataToServer();
                    }
                }

            } else {
                uploadFailed();
            }


        } catch (Exception e) {
            e.printStackTrace();
            uploadFailed();
        }

        return "";
    }

    protected void uploadFailed() {
        ((ShopDataViewPagerHost) context).postFeedCompleted("", false);
    }


}
