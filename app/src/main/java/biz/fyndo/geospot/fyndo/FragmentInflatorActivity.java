package biz.fyndo.geospot.fyndo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by vibinvarghese on 02/09/16.
 */
public class FragmentInflatorActivity extends AppCompatActivity {

    ProfileInfoFragment profileInfoFragment;
    PostFragment postFragment;
    FeedsFragment feedsFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String fragmentName = getIntent().getStringExtra("fragmentName");

        this.getSupportActionBar().hide();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (fragmentName == null)
            return;

        switch (fragmentName) {
            case "ProfileInfoFragment":
                profileInfoFragment = new ProfileInfoFragment(true);
                fragmentTransaction.replace(android.R.id.content, profileInfoFragment);
                fragmentTransaction.commit();
                break;
            case "PostFragment":
                postFragment = new PostFragment();
                fragmentTransaction.replace(android.R.id.content, postFragment);
                fragmentTransaction.commit();
                break;
            case "FeedsFragment":
                feedsFragment = new FeedsFragment();
                fragmentTransaction.replace(android.R.id.content, feedsFragment);
                fragmentTransaction.commit();
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 456) {
            postFragment.processImageCapture(requestCode, resultCode, data);
        }
    }

    protected void postUserBizInfoUpdated(String response, boolean isSuccess) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                profileInfoFragment.progressBar.setVisibility(View.GONE);
                profileInfoFragment.actionButton.setVisibility(View.VISIBLE);
            }
        });

        if(!isSuccess) {
            Util.showSnackBar("Something went wrong. Please try again.", profileInfoFragment.shopPhone);
            return;
        }

        Util.showSnackBar("Profile Updated Successfully", profileInfoFragment.shopPhone);
    }

    protected void getFeedsFromUserId(String response, boolean isSuccess) {


        if(!isSuccess) {
            Util.showSnackBar("Something went wrong. Please try again.", feedsFragment.shopArea);
            return;
        }

        feedsFragment.populateFetchedFeeds(response, isSuccess);
    }

    protected void postFeedCompleted(String response, boolean isSuccess) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                postFragment.content.setText("");
                postFragment.title.setText("");
                postFragment.image1.setImageDrawable(getResources().getDrawable(R.drawable.image_place_holder));
                postFragment.image2.setImageDrawable(getResources().getDrawable(R.drawable.image_place_holder));
                postFragment.image3.setImageDrawable(getResources().getDrawable(R.drawable.image_place_holder));
                /*postFragment.image1.setColorFilter(getResources().getColor(android.R.color.darker_gray), android.graphics.PorterDuff.Mode.SRC_IN);
                postFragment.image2.setColorFilter(getResources().getColor(android.R.color.darker_gray), android.graphics.PorterDuff.Mode.SRC_IN);
                postFragment.image3.setColorFilter(getResources().getColor(android.R.color.darker_gray), android.graphics.PorterDuff.Mode.SRC_IN);*/
                postFragment.progressBar.setVisibility(View.GONE);
                postFragment.postButton.setVisibility(View.VISIBLE);

                ShopDataActivity.changeTab(2);
            }
        });

        if(!isSuccess) {
            Util.showSnackBar("Something went wrong. Please try again.", postFragment.postButton);
            return;
        }

        Util.showSnackBar("Post Uploaded Successfully", postFragment.postButton);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
