package biz.fyndo.geospot.fyndo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by vibinvarghese on 08/09/16.
 */

public class PerformAsyncTask extends AsyncTask {

    private Context context;
    private String callingActivity, requestURL, method;
    private JSONObject postDataParams;

    public PerformAsyncTask(Context context, String callingActivity, String requestURL,
                            JSONObject postDataParams, String method) {
        super();
        this.context = context;
        this.callingActivity = callingActivity;
        this.requestURL = requestURL;
        this.postDataParams = postDataParams;
        this.method = method;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }

    @Override
    protected Object doInBackground(Object[] params) {

        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);

            if (method.equals("POST")) {
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestMethod(method);
                conn.setRequestProperty("Content-Type", "application/json");
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(postDataParams.toString());

                writer.flush();
                writer.close();
                os.close();
            }

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
                callBackAfterNetworkResponse(response, true);
            } else {
                callBackAfterNetworkResponse(response, false);
            }

        } catch (Exception e) {
            callBackAfterNetworkResponse(response, false);
            e.printStackTrace();
        }

        return response;
    }

    private void callBackAfterNetworkResponse(String response, boolean isSuccess) {
        switch (callingActivity) {
            case "MainActivity":
                ((MainActivity) context).postUserCreated(response, isSuccess);
                break;
            case "CheckPhoneNumber":
                ((MainActivity) context).checkIfUserExists(response, isSuccess);
                break;
            case "InsertProfileInfoFragment":
                if (!isSuccess) {
                    Toast.makeText(context, "Something went wrong please try again", Toast.LENGTH_LONG).show();
                    break;
                }
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
                editor.putInt(Constants.BIZ_ID, Integer.parseInt(response));
                editor.apply();
                /*Intent intent = new Intent(context, ShopDataViewPagerHost.class);
                context.startActivity(intent);*/
                ((MainActivity) context).launchSubCategorySelectorFragment();
                break;
            case "UpdateProfileInfoFragment":
                ((ShopDataViewPagerHost) context).postUserBizInfoUpdated(response, isSuccess);
                break;
            case "InsertRawCategoryObject":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).processRawSubcatCreation();
                    }
                });
                break;
            case "PostFeedFragment":
                ((ShopDataViewPagerHost) context).postFeedCompleted(response, isSuccess);
                break;
            case "FeedsFragment":
                ((ShopDataViewPagerHost) context).getFeedsFromUserId(response, isSuccess);
                break;
            case "GetBizAreas":
                ((MainActivity) context).saveBizAreas(response, isSuccess);
                break;
            case "GetBizCategories":
                ((MainActivity) context).saveBizCategories(response, isSuccess);
                break;
            case "DeleteFeed":
                ((ShopDataViewPagerHost) context).postDeleteFeed(response, isSuccess);
                break;
        }
    }
}
