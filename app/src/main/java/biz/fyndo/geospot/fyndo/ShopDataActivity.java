package biz.fyndo.geospot.fyndo;

import android.*;
import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.io.IOException;

/**
 * Created by vibinvarghese on 30/08/16.
 */
public class ShopDataActivity extends TabActivity {

    static TabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shop_data_activity);

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getApplicationContext();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("ShopDataActivity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        View divider = findViewById(R.id.divider);

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(divider.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS, 0);

        addTab("Profile", R.drawable.profile_tab, "ProfileInfoFragment");
        addTab("Post", R.drawable.post_tab, "PostFragment");
        addTab("Feed", R.drawable.feed_tab, "FeedsFragment");

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    private void addTab(String label, int drawableId, String fragmentName) {
        tabHost = getTabHost();

        Intent intent = new Intent(this, FragmentInflatorActivity.class);
        intent.putExtra("fragmentName", fragmentName);
        TabHost.TabSpec spec = tabHost.newTabSpec(label);

        View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
        TextView title = (TextView) tabIndicator.findViewById(R.id.title);
        title.setText(label);
        ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon);
        icon.setImageResource(drawableId);

        spec.setIndicator(tabIndicator);
        spec.setContent(intent);
        tabHost.addTab(spec);

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {

                for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
                    TextView title = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(R.id.title);
                    ImageView image = (ImageView) tabHost.getTabWidget().getChildAt(i).findViewById(R.id.icon);
                    title.setTextColor(getResources().getColor(R.color.divider));
                    image.setColorFilter(getResources().getColor(R.color.divider));
                }

                TextView title = (TextView) tabHost.getCurrentTabView().findViewById(R.id.title);
                ImageView image = (ImageView) tabHost.getCurrentTabView().findViewById(R.id.icon);
                title.setTextColor(getResources().getColor(R.color.colorPrimary));
                image.setColorFilter(getResources().getColor(R.color.colorPrimary));
            }
        });

        tabHost.setCurrentTab(1);
    }

    protected static void changeTab(int tab) {
        tabHost.setCurrentTab(tab);
    }
}
