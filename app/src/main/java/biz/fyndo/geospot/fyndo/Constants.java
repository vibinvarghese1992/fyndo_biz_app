package biz.fyndo.geospot.fyndo;

/**
 * Created by vibinvarghese on 06/09/16.
 */

public class Constants {
    public static final String NEXMO_APP_ID = "f994b919-1d42-476a-b924-e4267c3be3df";
    public static final String NEXMO_SHARED_SECRET = "251e1bff8ea54a2";

    public static final String IS_LOGGED_IN = "isLoggedIn";
    public static final String IS_PROFILE_FILLED = "isProfileFilled";
    public static final String USER_ID = "user_id";
    public static final String BIZ_ID = "biz_id";
    public static final String USER_PREFERENCES_MEN = "user_preferences_men";
    public static final String USER_PREFERENCES_WOMEN = "user_preferences_women";

    public static final String ROOT_URL = "http://fyndox.herokuapp.com";
    public static final String BASE_URL = "http://fyndox.herokuapp.com/api/";
    public static final String INSERT_USER = BASE_URL + "insertUser";
    public static final String INSERT_BIZ_INFO = BASE_URL + "insertBizInfo";
    public static final String UPDATE_BIZ_INFO = BASE_URL + "updateUserWithBizInfo";
    public static final String INSERT_FEED = BASE_URL + "insertFeed";
    public static final String SAVE_FILE = "http://fyndox.herokuapp.com/saveFile";
    public static final String SELECT_FEED_BY_BIZ_ID = BASE_URL + "selectFeedsByBizId/";
    public static final String SELECT_BIZ_AREAS = BASE_URL + "selectBizAreas/";
    public static final String SELECT_BIZ_CATEGORIES = BASE_URL + "selectBizCategories/";
    public static final String SELECT_USER_BY_PHONE = BASE_URL + "selectUserWithBizInfoByPhone/";
    public static final String INSERT_BIZ_CATEGORY = BASE_URL + "insertBizCategory/";
    public static final String DELETE_FEED = BASE_URL + "deleteFeed/";

    public static final String SHOP_NAME = "shop_name";
    public static final String SHOP_DESCRIPTION = "shop_description";
    public static final String SHOP_PHONE = "shop_phone";
    public static final String SHOP_TYPE = "shop_type";
    public static final String SHOP_AREA = "shop_area";
    public static final String SHOP_AREA_SERVER = "shop_area_server";
    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PHONE = "user_phone";
    public static final String USER_PROFILE_IMAGE = "user_profile_image";
    public static final String USER_RELATION_TO_STORE = "user_relation_to_store";
    public static final String SHOP_CATEGORY = "shop_category";
    public static final String SHOP_CATEGORY_SERVER = "shop_category_server";
    public static final String SHOP_PROFILE_IMAGE = "shop_profile_image";
    public static final String BIZ_AREA_LIST = "biz_area_list";
    public static final String SHOP_CATEGORY_STORE = "shop_category_store";
    public static final String SHOP_CATEGORY_EATERY = "shop_category_eatery";
    public static final String IS_FIRST_LAUNCH = "is_first_launch";

    public static final String SAVED_PREFS_MEN = "saved_prefs_men";
    public static final String SAVED_PREFS_WOMEN = "saved_prefs_women";


}
