package biz.fyndo.geospot.fyndo;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vibinvarghese on 31/08/16.
 */
public class FeedsFragment extends Fragment {

    SharedPreferences prefs;

    String storedShopArea, storedShopName;

    ProgressBar progressBar;
    ImageView shopImage;

    TextView shopName, shopArea, noPostCopy;
    RecyclerView recyclerView;

    ArrayList<FeedItemModel> feedArrayList;
    FeedListRecyclerAdapter feedListRecyclerAdapter;

    @Override
    public void onResume() {
        super.onResume();
        updateShopData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.feeds_fragment, container, false);

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getContext().getApplicationContext();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("FeedsFragment");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        shopName = (TextView) rootView.findViewById(R.id.shop_name);
        noPostCopy = (TextView) rootView.findViewById(R.id.no_post_copy);
        shopArea = (TextView) rootView.findViewById(R.id.shop_area);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress);
        shopImage = (ImageView) rootView.findViewById(R.id.shop_icon);

        String shopImagePath = prefs.getString(Constants.SHOP_PROFILE_IMAGE, "");
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        updateShopData();

        feedArrayList = new ArrayList<>();

        feedListRecyclerAdapter = new FeedListRecyclerAdapter(getActivity(), feedArrayList, "", true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(feedListRecyclerAdapter);

        return rootView;
    }

    protected void populateFetchedFeeds(String response, boolean isSuccess) {
        if (!isSuccess) {
            Util.showSnackBar("Something went wrong please try again", shopArea);
            return;
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

            }
        });

        try {
            populateRecyclerView(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void populateRecyclerView(String response) throws JSONException {
        JSONArray posts = new JSONArray(response);

        feedArrayList.clear();

        if (feedArrayList.size() == 0) {

            getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                noPostCopy.setVisibility(View.VISIBLE);
                                            }
                                        }
            );
        }

        //feedArrayList.clear();

        for (int i = 0; i < posts.length(); i++) {

            JSONObject feed = posts.getJSONObject(i);
            if (feed.isNull("linkedFiles"))
                continue;

            FeedItemModel feedItemModel = new FeedItemModel();
            feedItemModel.setTitle(feed.getString("title"));
            int id = feed.getInt("feedId");

            feedItemModel.setRating((int) feed.getDouble("userRating"));

            if (!feedItemModel.getTitle().contains("[")) {
                feedItemModel.setMainTitle(feedItemModel.getTitle());
            }

            if (!feed.isNull("bizInfo")) {
                feedItemModel.setShopImage(feed.getJSONObject("bizInfo").getString("bizProfile"));
            } else {
                feedItemModel.setShopImage("");
            }

            feedItemModel.setMainDescription(feed.getString("textContent"));

            feedItemModel.setLat(feed.getDouble("gpsLatitude"));
            feedItemModel.setLng(feed.getDouble("gpsLongitude"));
            feedItemModel.setMainCategory(feed.getString("sourceDevice"));

            feedItemModel.setId(id);
            feedItemModel.setDateOfPost(feed.getLong("dateOfPost"));
            feedItemModel.setDescription(feed.getString("sourceDevice"));
            feedItemModel.setImageURLs(feed.getJSONArray("linkedFiles"));
            feedItemModel.setSubCategories(feed.getJSONArray("userCatLevel2"));
            feedItemModel.setDealType(feed.getString("feedType"));
            String validity = "1";
            if (feed.getInt("numExpiryDays") > 0) {
                validity = String.valueOf(feed.getInt("numExpiryDays"));
            }
            feedItemModel.setValidity(validity);

            feedItemModel.setLikes(String.valueOf(feed.getInt("noOfUpvotes")));

            feedItemModel.setShopName("");
            feedItemModel.setShopArea("");
            feedItemModel.setBizId(feed.getInt("bizId"));

            if (!feed.isNull("shopName"))
                feedItemModel.setShopName(feed.getString("shopName"));

            if (!feed.isNull("shopArea"))
                feedItemModel.setShopArea(feed.getString("shopArea"));

            feedArrayList.add(feedItemModel);
        }

        if (getActivity() != null)
            getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                progressBar.setVisibility(View.GONE);
                                                recyclerView.setVisibility(View.VISIBLE);
                                                feedListRecyclerAdapter.notifyDataSetChanged();
                                                //feedListRecyclerAdapter.notifyItemRangeChanged(0, feedArrayList.size());

                                            }
                                        }
            );
    }

    public void updateShopData() {

        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        noPostCopy.setVisibility(View.GONE);

        Gson gson = new Gson();
        String json = prefs.getString(Constants.BIZ_AREA_LIST, null);
        Type type = new TypeToken<List<String>>() {
        }.getType();
        ArrayList<String> shopAreaList = gson.fromJson(json, type);
        storedShopName = prefs.getString(Constants.SHOP_NAME, "Shop Name");
        int shopAreaListPosition = prefs.getInt(Constants.SHOP_AREA, 0);
        storedShopArea = shopAreaList.get(shopAreaListPosition);

        shopName.setText(storedShopName);
        shopArea.setText(storedShopArea);

        String shopImagePath = prefs.getString(Constants.SHOP_PROFILE_IMAGE, "");

        if (!shopImagePath.equals("")) {
            Picasso.with(getActivity()).load(Constants.ROOT_URL + shopImagePath)
                    .resize(80, 80)
                    .centerCrop()
                    .placeholder(R.drawable.store_place_holder)
                    .into(shopImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) shopImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getWidth()) / 2.0f);
                            shopImage.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError() {
                            shopImage.setImageResource(R.drawable.store);
                        }
                    });
            shopImage.setPadding(4, 4, 4, 4);
        }

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "FeedsFragment",
                Constants.SELECT_FEED_BY_BIZ_ID + prefs.getInt(Constants.BIZ_ID, 0), null, "GET");

        performAsyncTask.execute();

    }
}
